# Scenario 3 - Kubernetes

This will help to setup microsite counter application and monitoring cluster resources. 

## Configuring local machine
Installed below tools to have application running locally:

 * Installed ***_Docker-Desktop_*** & enabled ***_Kubernetes_***.  
 * Used choco to install helm and and added helm repo. Use below commands:
  ``` 
 choco install kubernetes-helm
  ``` 

## Application which takes <name> as environment variable

Docker image has been created and is publicly available, you can pull with below command 

```
docker pull ankits19/env_var_sg:latest
```
Below are sample output to run docker application which shows my firstname passed with environment variable.

######Sample outputs:

![env_name_docker](screenshots/env_name_docker.PNG)

## Deploy Microsite Counter Application On Kubernetes

Make sure you have docker and Kubernetes service running on your local machine.
Docker image has been created and is publicly available, you can pull with below command 

```
docker pull ankits19/devops_sg:latest
```

For this application to run smoothly and easily, I have already define docker image path in `deployment.yaml`.
Download deployment.yaml & service.yaml.

To deploy this application run below commands:

```buildoutcfg
kubectl apply -f kubernetes_files/microsite_dep.yaml
```
```buildoutcfg
kubectl apply -f kubernetes_files/microsite_svc.yaml
```
Deployment and Service deployment:
![deployment&service](screenshots/deployment_service_sc.PNG)

Pods status:
![pods](screenshots/pods_sc.PNG)
You should be able to access application on localhost

```buildoutcfg
http://localhost:30163
```

![Application on local host](screenshots/counter_application.PNG)

## Deployment of mysql database

To create root password for mysql database, making use of K8s secrets and referring in deployments.

```buildoutcfg
kubectl apply -f kubernetes_files/mysql_dep.yaml
```
Secret deployment:

![mysql_secret](screenshots/mysql_secret.PNG)
Using docker base image of mysql to deploy this in our cluster.

```buildoutcfg
kubectl apply -f kubernetes_files/mysql_dep.yaml
```

![mysql_secret](screenshots/mysql_deployment.PNG)

#### Creation of table in MYSQL server:

###### Accessing MYSQL server:
To create table we have to exec into mysql pod created, by below command:

* Replace <pod_name> with your pod name.
```
 kubectl exec --stdin --tty  <pod_name> -- /bin/bash 
```

Below command will create table with name `work_ankit`.

```buildoutcfg
CREATE TABLE work_ankit (name VARCHAR(20), company VARCHAR(20), Year YEAR);
```
![mysql_table_creation](screenshots/mysql_table_creation.PNG)

## Connectivity test from Microsite Counter to MySQL server:
I tested this connectivity with 'telnet' utility from one of the microsite application pod.

![mysql_secret](screenshots/connectivity_site_mysql.PNG)

## Installing prometheus and Grafana for monitoring

Using helm to install grafana and prometheus. To make sure we have latest chart available locally. We have to add repo and update chart under repo by following commands:
``` 
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts 
```
```
helm repo add stable https://charts.helm.sh/stable
```
```
helm repo update
```
![helm_add_repo](screenshots/helm_add_repo.PNG)

use below command to install prometheus from helm chart:
```
helm install prometheus prometheus-community/kube-prometheus-stack
```

![helm_add_repo](screenshots/prometheus_install.PNG)

Once Prometheus and Grafana is installed you can verify all resources created as part of chart deployment.

***Deployment created***:
![grafana_resources](screenshots/grafana_resources.PNG)

***Services created***:
![grafana_svc_resources](screenshots/grafana_svc_resources.PNG)

To access Grafana dashboard, you need to port forward application by below command:
```
kubectl port-forward deployment/prometheus-grafana 3000
```
![port_forwarding](screenshots/port_forwarding.PNG)

Once port forwarding enabled, you should be able to see grafana login page from `http://localhost:3000/login` 
![grafana_login](screenshots/grafana_login.PNG)

***Note:*** To login to Grafana dashboard

```
username - admin
password - prom-operator
```

Post login you should be able to see the home page,to achieve that you have to import dashboards from [Grafana dashboard](https://grafana.com/grafana/dashboards/ "Grafana's Dashboard")
Below are the few example from dashboards:

* ![grafana_1](screenshots/grafana_1.PNG)
* ![grafana_2](screenshots/grafana_2.PNG)

Below screenshots shows health number of nodes, pods, container per pods. 
* ![grafana_3](screenshots/grafana_3.PNG)

## Troubleshooting steps

