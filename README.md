# Cloud_DevOps_SG

This repo is created to store and share task which were assigned.
Repository consists of folder respective to each scenario. 

To work with all scenarios, you will need below tools configured in your local machine.

* Terraform
* Docker
* git
* aws cli
* Kubernetes
* curl  
* Your preferred IDE (VSCode, IntelliJ or anyother) for better code reading.

***Scenario 1:***

This folder contains terraform code to create AWS resources as per requested architecture.
To deploy resources with code provided, post cloning of code navigate to directory `terraform_code` under Scenario 1. 

```buildoutcfg
cd "Scenario 1 - Terraform and AWS/terraform_code/"
``` 
Please refer ***README.md*** under scenario folder for more information.

***Scenario 2:***

It is having files to deploy counter application in Gitlab pages.
I have deployed Gitlab page already for counter application. URL to access the application publicly

```buildoutcfg
https://ankits19.gitlab.io/cloud_devops_sg/
```

Please refer ***README.md*** under scenario folder for more information.

***Scenario 3:***

This folder consists of all the files to deploy Kubernetes resources requested in this scenario.
Also have custom dockerfile to deploy microsite counter application.  

To deploy resources with code provided, post cloning of code navigate to directory `kubernetes_files` under Scenario 3. 

```buildoutcfg
cd "Scenario 3 - Kubernetes/kubernetes_files/"
``` 

Please refer ***README.md*** under scenario folder for more information.
