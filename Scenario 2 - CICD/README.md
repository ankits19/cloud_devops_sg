# Scenario 2 - CICD
This will help to create Gitlab pages for static application.

## CI/CD Stages
Four stages are Source, Build, Testing & Deployment

***Source*** - This is the baseline unit of the CI phase automation involves monitoring the version control system for any changes.

***Build*** - This stage builds the environment to run the application.
It will include compilation, packaging.

***Test*** - This stage plays an important role in any application development. It helps to complete automated test before deploying application or updating application.

***Deploy*** - Once the build is done and automated tests for release completed, in this stage tested code is deployed to respective enviroment.  
###### Stages mentioned in `.gitlab-ci.yml`:

* **build**: This will copy all the application files from Scenario 2 folder to public folder, as GITLAB pages deploy application to internet public folder named `public`. 
  

* **test**: I am testing whether all the files are copied to `public` folder, by checking count of the files with help of artifacts generated in build stage.

* **deploy**: This stage I am deploying application and artifacts, using URL to access application. 

Below example showw 2 cases:

![stages_testing](screenshots/stages_testing.PNG)

**Case 2** - I intentionally added extra file in code repo, so my test case fails.

**Case 1** - Removed extra fake file to pass test stage eventually.


##Access application through Internet
Please use below URL to access application, which is deployed in GITLAB pages:

```buildoutcfg
https://ankits19.gitlab.io/cloud_devops_sg/
```

![gitlab_application](screenshots/application_gitlab_pages.PNG)

##Optional: Trigger Pipeline externally

You can trigger gitlab CI/CD Pipeline with below command,(assuming you have curl installed on you local machine)

```
curl -X POST -F token=dd2cd591bea1062cab40b9eeb24f8d -F ref=master https://gitlab.com/api/v4/projects/32245997/trigger/pipeline
```
* Token I have generated for interviewer to trigger application.

Once it is success trigger you should see below output:

![trigger_pipeling](screenshots/trigger_pipeline.PNG)

You can also verify triggered pipeline from gitlab repo URL by navigating to CI/CD -->Pipelines.
You should see triggered pipeline with latest timestamp.

![gitlab_pipeline](screenshots/gitlab_pipeline.PNG)
