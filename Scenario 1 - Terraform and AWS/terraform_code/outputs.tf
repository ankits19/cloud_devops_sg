output "public_server_ip" {
  description = "Public IP to access the Web server from internet"
  value = module.ec2_public_server.public_ip
}

output "private_server_ip" {
  description = "Private IP for ServerB"
  value = module.ec2_private_server.private_ip
}