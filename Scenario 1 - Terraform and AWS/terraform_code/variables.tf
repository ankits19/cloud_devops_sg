## Variables defined common for Public & Private server

variable "region" {
  description = "Region where to deploy resources"
  default = "ap-southeast-1"
  type = string
}
variable "vpc_id" {
  description = "Default VPC ID"
  type = string
}
variable "key_name" {
  description = "Key used to create Instances"
  type = string
}
variable "additional_tags" {
  description = "Additional resource tags"
  default     = {}
  type        = map(string)
}

## Variables defined for Public server

variable "web_server_instance_type" {
  description = "Instance type for web server"
  type = string
  default = "t2.micro"
}

variable "web_server_instance_name" {
  description = "Instance name for web server"
  type = string
}

variable "public_subnet_id" {
  description = "Public Subnet ID"
  type = string
}

variable "public_server_name" {
  description = "Name tag for public server"
  type = string
}
variable "public_server_ports" {
  description = "Ports for web server"
  type = list(number)
}
## Variables defined for Private server

variable "private_server_instance_type" {
  description = "Instance type for private server"
  type = string
  default = "t2.micro"
}
variable "private_server_instance_name" {
  description = "Instance name for private server"
  type = string
}
variable "private_subnet_id" {
  description = "Private Subnet ID"
  type = string
}
variable "private_server_name" {
  description = "Name tag for private server"
  type = string
}