module "ec2_public_server" {
  source = "../modules"
  ami_id = data.aws_ami.amazon_ami.id
  associate_public_ip_address = true
  instance_type = var.web_server_instance_type
  key_name = var.key_name
  subnet_id = var.public_subnet_id
  user_data = <<EOF
  #!/bin/bash
  echo "Installing Web server"
  sudo yum update -y
  sudo yum install -y httpd
  sudo service httpd start
EOF
  vpc_security_group_ids = [aws_security_group.public_server_sg.id]
  name_tag = {
    Name = var.public_server_name
  }
  additional_tags = var.additional_tags
  depends_on = [aws_security_group.public_server_sg]
}

module "ec2_private_server" {
  source = "../modules"
  ami_id = data.aws_ami.amazon_ami.id
  associate_public_ip_address = false
  instance_type = var.private_server_instance_type
  key_name = var.key_name
  subnet_id = var.private_subnet_id
  user_data = ""
  vpc_security_group_ids = [aws_security_group.private_server_sg.id]
  name_tag = {
    Name = var.private_server_name
  }
  additional_tags = var.additional_tags
  depends_on = [aws_security_group.private_server_sg]
}