resource "aws_security_group" "public_server_sg" {
  description = "Security group created for public server"
  vpc_id = var.vpc_id
  name = "public_server_sg"
  tags = merge(
    var.additional_tags,
    {
      Name = "public_server_sg"
    },
  )
}

resource "aws_security_group_rule" "allow_access_to_public_server" {
  count             = length(var.public_server_ports)
  description       = join(" ", ["Allowing webserver on port" , var.public_server_ports[count.index]] )
  type              = "ingress"
  from_port         = var.public_server_ports[count.index]
  to_port           = var.public_server_ports[count.index]
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.public_server_sg.id
}

resource "aws_security_group_rule" "allow_internet_access_from_public_server" {
  description       = "Allow internet access on Public server"
  type              = "egress"
  from_port         =  0
  to_port           =  0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.public_server_sg.id
}

resource "aws_security_group" "private_server_sg" {
  description = "Security group created for private server"
  name = "private_server_sg"
  vpc_id = var.vpc_id
  tags = merge(
    var.additional_tags,
    {
      Name = "private_server_sg"
    },
  )
}

resource "aws_security_group_rule" "allow_ssh_from_web_server" {
  type              = "ingress"
  description       = "Allowing SSH from ServerA"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  source_security_group_id = aws_security_group.public_server_sg.id
  security_group_id = aws_security_group.private_server_sg.id
}

#Creating Route table for Private subnet
resource "aws_route_table" "private_route_table" {
  vpc_id = var.vpc_id
  tags = merge(
    var.additional_tags,
    {
      Name = "private_subnet_rt"
    },
  )
}

resource "aws_route_table_association" "private_subnet_rt" {
  subnet_id      = var.private_subnet_id
  route_table_id = aws_route_table.private_route_table.id
}