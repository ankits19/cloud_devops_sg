#Region to deploy resources
region = "ap-southeast-1"

#Default VPC ID
vpc_id = "vpc-0527cbf5deecd2145"

#Key name
key_name = "devops_sg_key"

#Public Server inputs
web_server_instance_type = "t2.micro"
web_server_instance_name = "ServerA"
public_subnet_id = "subnet-035a68d5218d33540"
public_server_ports = [22 ,80]
public_server_name = "ServerA"

#Private Server inputs
private_server_instance_type = "t2.micro"
private_server_instance_name = "ServerB"
private_subnet_id = "subnet-0e90c386b3b0ffec0"
private_server_name = "ServerB"

#Additional Tags for all resources
additional_tags = {
  Scenario = "AWS and Terraform"
  Country = "Singapore"
}

