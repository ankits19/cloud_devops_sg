# Scenario 1 - Terraform and AWS

This will help to create resource in AWS Singapore region with terraform. 

![scenario_1](screenshots/scenario_1.PNG)

## Configuring local machine
 * Installed terraform to deploy resources using [link](https://learn.hashicorp.com/tutorials/terraform/install-cli "Install Terraform")
 * Installed aws cli to configure credentials for the account.  

##Launching AWS resources

Once you have code cloned in you local machine. You have to run below commands to deploy resources in your account.

***Note***: In this example I used `key_name = "devops_sg_key"`, Please update key_name in `terraform.tfvars` with key_name you have in you account and have access to before proceeding 
 
* `Terraform init` - This will download the plugins from registry.hashicorp.com for API calls for AWS.

![terraform_init](screenshots/terraform_init.PNG)

* `Terraform plan` - This will create an execution plan, which will let you know what changes you going to make to AWS Infrastructure.

![terraform_plan](screenshots/terraform_plan.PNG)

* `Terraform apply` - This command will make real changes to AWS infrastructure.
It will create below resources:
  
  * 2 Instances; ***ServerA*** & ***ServerB***
  * ServerA will be accessible from internet on port ***22*** and ***80***
  * ServerB will be only accessible from ***ServerA*** on port ***22***
  * 2 security groups; ***public_server_sg*** & ***public_server_sg***
  * Route table for private subnet as we are using default VPC, which have all subnet as public by default.
    
![terraform_apply](screenshots/terraform_apply.PNG)

Screenshot from AWS console
![aws_console](screenshots/aws_apply.PNG)

Post resources are created you will seeing Public IP of ServerA like below:

![outputs](screenshots/apply_output.PNG)

As we are installing Webserver on the server you will be able to see webserver default page on browser `http://<public_ip>`

#####Validation of SSH and HTTP from internet:
![ssh_validation](screenshots/ssh_from_internet.PNG)
![validation](screenshots/ssh_http_from_internet.PNG)

####Access Private server from Public server:

To do this from public server from we have to copy private key with help of WinSCP (or similar tools)

![ssh_to_private_server](screenshots/ssh_to_private_server.PNG)

##Troubleshooting steps

* Make sure your IAM credentials configured has access to AWS resources.
* Terraform installed and $PATH is updated accordingly.

## Cleanup steps

Run `terraform destroy` to delete all the resources created from code.

![cleanup_resources](screenshots/destroy_tf.PNG)
![cleanup_resources_2](screenshots/destroyed_resources.PNG)


