resource "aws_instance" "ec2" {
  ami = var.ami_id
  instance_type = var.instance_type
  subnet_id = var.subnet_id
  key_name = var.key_name
  associate_public_ip_address = var.associate_public_ip_address
  vpc_security_group_ids = var.vpc_security_group_ids
  user_data = var.user_data
  tags = merge(
    var.name_tag, var.additional_tags
  )
}