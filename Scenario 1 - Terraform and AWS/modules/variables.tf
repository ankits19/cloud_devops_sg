variable "ami_id" {
  description = "AMI ID for server"
  type = string
}
variable "instance_type" {
  description = "Instance type for server"
  type = string
}
variable "subnet_id" {
  description = "Subnet ID for server"
  type = string
}
variable "key_name" {
  description = "Key name for server"
  type = string
}
variable "associate_public_ip_address" {
  description = "Public IP address association"
  type = bool
}
variable "vpc_security_group_ids" {
  description = "Security groups for server"
  type = list(string)
}
variable "user_data" {
  description = "User data for server"
  type = string
}
variable "name_tag" {
  description = "Name tag for server"
  type = map(string)
}

variable "additional_tags" {
  description = "Additional tags for server"
  type = map(string)
}